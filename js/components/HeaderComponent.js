
export default {
  template: `
  <div>
	<!-- Header section -->
	<header class="header-section">
		<div class="logo">
			<img src="img/mainLogo.svg" alt="Main Logo of Ontario Summer"><!-- Logo -->
		</div>
		<!-- Navigation -->
		<div v-on:click="responsive" class="responsive"><i class="fa fa-bars"></i></div>
		<nav id="main-navbar">
			<h2 class="hide">Main Navigation</h2>
			<ul class="menu-list">
				<li class="active"><a href="#/home">Home</a></li>
				<li><a href="#aboutOn" class="textChange">About ONTARIO</a></li>
				<li><a href="#galBlock" class="textChange">Gallery</a></li>
				<li><a href="#/register" class="textChange">Sign Up</a></li>
			</ul>
		</nav>
	</header>
	<!-- Header section end -->
	</div>
  `,

  data() {
    return {};
	},
	
  mounted: function(){
 
    // //waypoint functions
  
  var waypoint = new Waypoint({
    element: document.querySelector('.header-section'),
    handler: function(direction) {
    console.log("a");
    const mainNav = document.querySelector(".menu-list");
    const header = document.querySelector('.header-section');
    const navText = document.querySelectorAll('.textChange');
    
    mainNav.classList.toggle('menubkgrd');
    header.classList.toggle('sticky-top');
    navText.forEach(function(text){
      text.classList.toggle('textColorChange');
    });
    
    
    },
    offset: -70
    })

  },

  methods: {
  
  // //nav button function

  responsive(){
  document.querySelector(".menu-list").classList.toggle('menuShow');
	},

  
  
  },

  components: {

  }
};
