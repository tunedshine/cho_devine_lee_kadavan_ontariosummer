# hackathon

Hackathon Project

The site is built to introduce a number of tourist spots, events, foods, anythigns that travellers can experiece in Ontario Province. The main purpose of this website is attracting people to travel Ontario Province.

(img/re1.png)

The heading section on index page shows a images slides of world famous attractions of Ontarion Province,

(img/re2.png)
      
The Card section on index page shows Some promotional text about why the Ontario is a best province to tour in the world.

(img/re3.png)

The Video Section on index page shows a variety of cities of Ontario Province for 35 seconds.

(img/re4.png)

The Gallery Section on index page shows a number of attractions of different cities in Ontario





## Built With

* [Foundation 6](https://foundation.zurb.com/sites.html) - The web framework used - Zurb Foundation 6 for sites
* [Git](https://git-scm.com/) Used for versioning.
* [Vue.js](https://vuejs.org/) to fetch database and put on html.
* [PHP/MySQL] to build database of the site.
* [Gulp.js](https://gulpjs.com/) Used sass, serve with Gulp
* [SASS](https://sass-lang.com/) Sass to manipulate CSS.
* [GreenSock](https://greensock.com/) - for animations of svg, text and image.
* [npm](https://www.npmjs.com/) npm compiled and install packages.
* [waypoints](http://imakewebthings.com/waypoints/) to trigger function on scroll to certain point.


## Authors


* **Reece Devine (Jim)** - *Project Manager*
* **JeongYun Cho (Sam)** - *Developer(Front-End, Back-End)*
* **Changseok Lee (Lee)** - *Developer(Back-End)*
* **Reece Devine (Jim)** - *Motion Designer*
* **Jeril G Kadavan(Jeril)** - *Designer*



## Acknowledgments

* images on google
* pinterest (https://www.pinterest.ca/)
* awwwards (https://www.awwwards.com/)
* previous work from graduated students

## Future Developments

* More Detailed information about more cities will be updated continuously
* Detail Explanation Page of various attractions wiil be updated
* Images of the sites could be modified better.
* Overall user experience can be improved will adding or removing animations of the website.

