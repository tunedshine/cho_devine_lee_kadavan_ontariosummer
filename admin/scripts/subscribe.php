<?php


function getSubscriber($fname, $lname, $email, $country)
{

    // connection to Database
    require_once 'config.php';
    $pdo = connect_db();
    
   
    $check_email_exist ="SELECT `user_email` FROM `tbl_user` WHERE `user_email` = :email";
    $check_email = $pdo->prepare($check_sub_exists_query);
    $check_email->execute(
       array(
         ":email" => $email
       )
       );
       
          if ( $check_email->fetchColumn()) {

        function subscribed_before_email($lname,$fname, $email){
          $to = $email;
          $subject = 'Subscription status';
          $body = $fname .' '.$lname .' you are already subscribed!';
          mail($to, $subject, $body);
       }

        
        $check_user_query ="SELECT `user_id` FROM `tbl_user` WHERE `user_email` = :email";
        $get_user_id = $pdo->prepare($check_user_query);
        $get_user_id->execute(
          array(
            ":email" => $email
          )
          );
        $user_id = $get_user_id->fetchColumn();
         

        //Update
        $update_sub_query = "UPDATE tbl_user SET `user_fname` = :fname, `user_lname` = :lname, `user_email` = :email, `last_update` = NOW(), `user_country` = :country WHERE `user_id` = :id";
        $update_sub = $pdo->prepare($update_sub_query);
        $update_sub->execute(
           array(
             ":fname" => $fname,
             ":lname" => $lname,
             ":email" => $email,
             ":id" => $user_id
           )
           );

    } else {
        new_subscriber_email($lname, $fname, $email);
        // insert subscriber
        $sub_insert = "INSERT INTO `tbl_user` (user_fname, user_lname, user_email, subscribe_date) VALUES(:fname, :lname, :email, NOW());";

        $insert_user = $pdo->prepare($sub_insert);
        $insert_user->execute(
          
        array(
      ':fname' => $fname,
      ':lname' => $lname,
      ':email' => $email,
      ':country' => $country
    )
  );

  //TODO:    

    // confirmation email to new user
function subscribe_new_email($lname, $fname, $email)
{
    $to = $email;
    $subject = 'Subscription status';
    $body = $fname .' '.$lname .' you are a new subscriber!';

    mail($to, $subject, $body);
}

}