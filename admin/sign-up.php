<?php

require_once 'scripts/config.php';


    $fname = (trim($_POST['first-name']));
    $lname = (trim($_POST['last-name']));
    $email = (trim($_POST['email']));
    $country = (trim($_POST['countries']));

    if (empty($fname) || empty($lname) || empty($email) | empty($country)) {
        $message = "Check the field";
    } else {
        
        $result = createSubscriber($fname, $lname, $email, $country);
        $message = $result;
    }

    echo json_encode($result);

