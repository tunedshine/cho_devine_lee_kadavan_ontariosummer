<?php
require_once 'config.php';

// connection to Database
function connect_db()
{
    $database = new Database();
    $conn = $database->connect();
    return $conn;
}

// Redirect to location
function redirect_to($location)
{
    if ($location != null) {
        header('Location:'.$location);
        exit();
    }
}


