import HeaderComponent from "./HeaderComponent.js";
import FooterComponent from "./FooterComponent.js";
export default {
  template: `
  <!-- Sign In section -->
  <div>
  <HeaderComponent />

	<div id="signUp" class="contact-section spad fix">
             <div class="container">
                <div class="row">
                    <!-- Sign IN info -->
                    <div class="col-md-5 col-md-offset-1 contact-info col-push">
                        <div class="section-title2 left">
                            <h2><i class="fas fa-arrow-left"></i>  Sign Up Here  </h2>
                        </div>
                        <p>Cras ex mauris, ornare eget pretium sit amet, dignissim et turpis. Nunc nec maximus dui, vel suscipit dolor. Donec elementum velit a orci facilisis rutrum. </p>
                    </div>
                    <!-- Sign In form -->
                    <div class="col-md-6 col-pull" id="con_form">
                        <form class="form-class">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" name="fname" placeholder="Your First Name">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" name="lname" placeholder="Your Last Name">
                                </div>
                                <div class="col-sm-6">
                                        <input type="text" name="username" placeholder="Your User Name">
                                </div>
                                <div class="col-sm-6">
                                        <input type="text" name="password" placeholder="Your Password">
                                </div>
                                <div class="col-sm-12">
                                    <input type="text" name="email" placeholder="email">
                                    <input type="text" name="country" placeholder="country">
                                    <button type="submit" class="site-btn">Create User</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
     
        <!-- Sign In section end-->

        <div class="row">
            <div class="col-md-12"><hr></div>
        </div>

        <!-- Contact In section -->
	<div class="contact-section spad fix">
            <div class="container">
                <div class="row">
                    <!-- Contact info -->
                    <div class="col-md-5 col-md-offset-1 contact-info col-push">
                        <div class="section-title2 left">
                            <h2><i class="fas fa-arrow-left"></i>  Get In Touch </h2>
                        </div>
                        <p>Cras ex mauris, ornare eget pretium sit amet, dignissim et turpis. Nunc nec maximus dui, vel suscipit dolor. Donec elementum velit a orci facilisis rutrum. </p>
                    </div>
                    <!-- Contact form -->
                    <div class="col-md-6 col-pull">
                        <form class="form-class" id="con_form">
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" name="fname" placeholder="Your First Name">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" name="lname" placeholder="Your Last Name">
                                </div>
                                <div class="col-sm-12">
                                    <input type="text" name="email" placeholder="email">
                                    <button type="submit" class="site-btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact section end-->

  <FooterComponent />
  </div>
  `,

  data() {
    return {
      "firstname":"",
      "lastname":"",
      email:"",
      country:""
    };
  },

  methods: {
    create_subscriber() {
        let url = `./admin/sign-up.php`;
        fetch(url, {
          method: "POST",
          body: formData
        })
          .then(res => res.json())
          .then(data => {
            console.log(data);
            if (data == "Subscription Failed") {
              console.log("try again");
            } else {
              console.log("Subscription created");
            }
          })
          .catch(function (error) {
            console.log(error);
          });
    }
  },

  components: {
    HeaderComponent: HeaderComponent, 
    FooterComponent:FooterComponent
  }
};
