import HeaderComponent from "./HeaderComponent.js";
import FooterComponent from "./FooterComponent.js";
export default {
  template: `
  <div>
  <h1 class="hide">Ontario Summer</h1>
	<!-- Page Preloder -->
	<div id="land">
	<div id="preloder">
		<div class="loader">
			<img src="img/mainLogo2.svg" width="500" alt="Main Logo of the Site">
			<p>Loading.....</p>
		</div>
	</div>
</div>

<HeaderComponent />

        <div class="hero-content">
            <div class="hero-center">
                <h2>Find Yourself at Home</h2>
            </div>
	</div>
	
		
	<div id="MainImageSlide" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
		  <li data-target="#MainImageSlide" data-slide-to="0" class="active"></li>
		  <li data-target="#MainImageSlide" data-slide-to="1"></li>
		  <li data-target="#MainImageSlide" data-slide-to="2"></li>
		</ol>
		<div class="carousel-inner">
		  <div class="carousel-item active">
			<div id="slide1"></div>
		  </div>
		  <div class="carousel-item">
			<div id="slide2"></div>
		  </div>
		  <div class="carousel-item">
			<div id="slide3"></div>
		  </div>
		</div>
		<a class="carousel-control-prev" href="#MainImageSlide" role="button" data-slide="prev">
		  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		  <span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#MainImageSlide" role="button" data-slide="next">
		  <span class="carousel-control-next-icon" aria-hidden="true"></span>
		  <span class="sr-only">Next</span>
		</a>
	  </div>



	<!-- About section -->
	<div id="aboutOn" class="about-section" data-spy="scroll" data-target="#main-navbar" data-offset="0">
		<h2 class="hide">About Card Section</h2>
		<div class="overlay"></div>
		<div class="card-section">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-6">
						<div class="lab-card">
							<div class="icon">
								<i class="flaticon-023-flask"></i>
							</div>
							<h3>Nature Based</h3>
							<p>The Ontarion Province where famous for great nature is one of the most beautiful states in the world.</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="lab-card">
							<div class="icon">
								<i class="flaticon-011-compass"></i>
							</div>
							<h3>World Famous</h3>
							<p>There are lots of world famous tourist spots in Ontario such as CN TOWER, CASA LOMA, ROGERS CENTER, HUGE AQUARIUM.</p>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="lab-card">
							<div class="icon">
								<i class="flaticon-037-idea"></i>
							</div>
							<h3>Easy to Travel</h3>
							<p>Well Connected transportation makes travellers easy to arrive to their destination!!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- card section end-->


		<!-- About content -->
		<div class="about-contant">
			<div class="container">
				<div id="aboutContainer">
				<div class="section-title">
					<h2>EXPLORE <span>THE NATURE </span> OF CANADA ONTARIO</h2>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p>Even though, The number of tourists who travel to Ontario Province have been increased a lot, still lots of people don't know about the beauty of Ontario.</p>
					</div>
					<div class="col-md-6">
						<p>Through this tourism Website, users can easily see how Ontario is beautiful and it will help them to make a awesome tour plan before they explore!!</p>
					</div>
				</div>
				</div>

				<!-- video -->
				<div class="intro-video">
					<h2 class="hide">Ontario Summer Tourism Promo Video</h2>
					<div class="row justify-content-center">
						<div class="col-md-8">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/1ODpQyGW4W0" allowfullscreen></iframe>
								  </div>
                  
						</div>
					</div>
				</div>

			</div>
		</div>
		
	</div>
	<!-- About section end -->
	
	<!-- Gallery Section-->
<!-- Gallery Section-->
<div id="Gal"> 
		
	
	<div id="mainGal">
	
	<div class="row">
		<div class="col-md-8 offset-md-2 text-center">
		<h2 id="galHead">CITIES IN ONTARIO</h2>
		</div>
	</div>

	<div id="citiesBkgrd" class="row">
			<div class="col-md-12 d-flex justify-content-center mb-5">

					<button type="button" class="btn btn-outline-black waves-effect filter" data-rel="all">TORONTO</button>
					<button type="button" class="btn btn-outline-black waves-effect filter" data-rel="1">OTTAWA</button>
					<button type="button" class="btn btn-outline-black waves-effect filter" data-rel="2">KINGSTON</button>
				
			</div>
	</div>

	<div id="galBlock" class="gallery-block" data-spy="scroll" data-target="#main-navbar" data-offset="0">
			<div>
				 <div class="row no-gutters">
					 
					<div class="col-md-6 col-lg-4 item zoom-on-hover">
								<div class="popContainer">
									<img class="productImage img-fluid" src="img/1.png" alt="Toronto">
									<div id="myModal" class="modal">
										<span v-on:click="closeBox" class="close">&times;</span>
										<img class="modal-content" id="img01" src="img/01.jpg" alt="modal Image">
									</div>
									<div id="0" class="popOverlay" v-on:click="swapImage($event)">
										<div class="popText">CN Tower
									
										</div>
										</div>
										</div>
					
								
						

					</div>

				 
				<div class="col-md-6 col-lg-4 item zoom-on-hover">
				
						<div class="popContainer">
								<img class="productImage img-fluid" src="img/2.png" alt="City 2">
								<div id="1" class="popOverlay" v-on:click="swapImage($event)">
									<div class="popText">Niagara Falls
									</div>
									</div>
			
						</div>
					</div>
					
				<div class="col-md-6 col-lg-4 item zoom-on-hover">
					
						<div class="popContainer">
								<img class="productImage img-fluid" src="img/3.png" alt="City 3">
								<div id="2" class="popOverlay" v-on:click="swapImage($event)">
									<div class="popText">Ottawa
									</div>
									</div>
			
						</div>
					</div>
		
					
					<div class="col-md-6 col-lg-4 item zoom-on-hover">
						<div class="popContainer">
								<img class="productImage img-fluid" src="img/02.jpg" alt="City 4">
								<div id="3" class="popOverlay" v-on:click="swapImage($event)">
									<div class="popText">Ontario Lake
									</div>
									</div>
			
						</div>
					</div>
					
					
					<div class="col-md-6 col-lg-4 item zoom-on-hover">
						<div class="popContainer">
								<img class="productImage img-fluid" src="img/5.png" alt="City 5">
								<div id="4" class="popOverlay" v-on:click="swapImage($event)">
									<div class="popText">Toronto
									</div>
									</div>
			
						</div>
					</div>
					
					
					<div class="col-md-6 col-lg-4 item zoom-on-hover">
						<div class="popContainer">
								<img class="productImage img-fluid" src="img/6.png" alt="City 6">
								<div id="5" class="popOverlay" v-on:click="swapImage($event)">
									<div class="popText">Casa Loma
									</div>
									</div>
			
						</div>
					</div>
					
		 
				 </div>
			</div>
		</div>

</div>

</div><!-- Gallery Section Ends  -->
<FooterComponent />
</div>


  `,

  data() {
    return {};
  },
  mounted: function(){
    
    fadeOutEffect(".loader",500);
    function fadeOutEffect(target,speed) {
      var fadeTarget = document.querySelector(target);
      var fadeEffect = setInterval(function () {
          if (!fadeTarget.style.opacity) {
              fadeTarget.style.opacity = 1;
          }
          if (fadeTarget.style.opacity > 0.8) {
              fadeTarget.style.opacity -= 0.1;
          } else {
      fadeTarget.style.opacity = 0;
      clearInterval(fadeEffect);
       fadeOutEffect("#land",220);
          }
    }, speed);
    
    };
    // waypoint functions

  },

  methods: {
  

  //Get the modal


// Get the image and insert it inside the modal - use its "alt" text as a caption

swapImage(event) {
  var modal = document.querySelector('#myModal');
  var modalImg = document.querySelector("#img01");
  var images = document.querySelectorAll('.productImage');
  var targetId = event.currentTarget.id;
  console.log(targetId);
    modal.style.display = "block";
        for(var i=0; i<images.length; i++){
            if(targetId==i){
            modalImg.src=images[i].src;
        }
    }
},



// When the user clicks on <span> (x), close the modal

closeBox(){
  document.querySelector('#myModal').style.display = "none";
}
  

  
  
  
  },

  components: {
    HeaderComponent: HeaderComponent, 
    FooterComponent:FooterComponent
  }
};
